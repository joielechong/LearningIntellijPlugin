import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import org.jetbrains.annotations.NotNull;

public class HelloAction extends AnAction {

    public HelloAction() {
        super("Hello");
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project project = e.getProject();
//        Messages.showMessageDialog(project, "Hello World!", "Greeting", Messages.getInformationIcon());
        NotificationGroup notif = new NotificationGroup("myplugin", NotificationDisplayType.BALLOON, true);
        notif.createNotification("My Title",
                "My Message",
                NotificationType.INFORMATION,
                null).notify(project);
    }
}
